# phpldapadmin

[docker-phpLDAPadmin](https://github.com/osixia/docker-phpLDAPadmin) provides LDAP.

## Access

It is available at [https://phpldapadmin.{{ domain }}/](https://phpldapadmin.{{ domain }}/) or [http://phpldapadmin.{{ domain }}/](http://phpldapadmin.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://phpldapadmin.{{ tor_domain }}/](http://phpldapadmin.{{ tor_domain }}/)
{% endif %}
