# Snibox

[Snibox](https://snibox.github.io/) Self-hosted snippet manager. Developed to collect and organize code snippets.

## Access

It is available at [https://snibox.{{ domain }}/](https://snibox.{{ domain }}/) or [http://snibox.{{ domain }}/](http://snibox.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://snibox.{{ tor_domain }}/](http://snibox.{{ tor_domain }}/)
{% endif %}
