# Grocy

[Grocy](https://grocy.info) ERP beyond your fridge - grocy is a web-based self-hosted groceries & household management solution for your home

## Access

Default login is user admin with password admin, please change the password immediately (see user menu).

It is available at [https://grocy.{{ domain }}/](https://grocy.{{ domain }}/) or [http://grocy.{{ domain }}/](http://grocy.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://grocy.{{ tor_domain }}/](http://grocy.{{ tor_domain }}/)
{% endif %}
