# SickChill

[SickChill](https://sickchill.github.io/) SickChill is an automatic Video Library Manager for TV Shows.

## Access

It is available at [https://sickchill.{{ domain }}/](https://sickchill.{{ domain }}/) or [http://sickchill.{{ domain }}/](http://sickchill.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://sickchill.{{ tor_domain }}/](http://sickchill.{{ tor_domain }}/)
{% endif %}
